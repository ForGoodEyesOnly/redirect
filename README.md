# HTTP redirects for forgoodeyesonly.eu

This repo solely exists for hosting HTML pages redirecting the user to certain URLs.
Due to the lack of artistic creativity involved in the creation, this repo is not copyright-protected.